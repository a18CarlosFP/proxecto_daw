<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductoController;
use App\Http\Controllers\RestauranteController;
use App\Http\Controllers\RestauranteProductoController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name("inicio");

Route::get('/proyecto', function () {
    return view('proyecto.index');
})->name("index");

Route::get('/restaurantes', function () {
    return view('index');
})->name("listaRestaurantes");

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('welcome');
});


Route::resource("restaurantes", RestauranteController::class)->middleware('auth');
Route::resource("producto", ProductoController::class);

Route::get('/allRestaurantes', [RestauranteController::class, 'allRestaurantes'])->name('restaurante.allRestaurantes');

Route::get('/listaProductos/{restaurante}', [RestauranteController::class, 'listaProductos'])->name('restaurante.listaProductos');

// Route::delete('/deleteRP/{restaurante}/{producto}', [RestauranteProductoController::class, 'deleteRP'])->name('restauranteProducto.deleteRP');
// Route::get('/añadirProductoRestaurante/{restaurante}', [RestauranteProductoController::class, 'añadirProductoRestaurante'])->name('restauranteProducto.añadirProductoRestaurante');
// Route::post("/storeProducto/{restaurante}", [RestauranteProductoController::class, 'storeProducto'])->name("restauranteProducto.storeProducto")->middleware('auth');
