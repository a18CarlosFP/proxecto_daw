<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("id_user");
            $table->unsignedBigInteger("id_restaurante");
            $table->dateTime("fechaHora_pedido", $precision = 0);
            $table->dateTime("fechaHora_entrega", $precision = 0);
            $table->timestamps();

            $table->foreign("id_user")->references("id")->on("users");
            $table->foreign("id_restaurante")->references("id")->on("restaurantes");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
};
