<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //'nombre', 'descripcion', 'precio', 'foto'
        Schema::create('productos', function (Blueprint $table) {
            $table->id();
            $table->string("nombre",100)->comment("Nombre Producto");
            $table->string("descripcion",200)->commnent("Descripcion del producto");
            $table->integer("precio")->comment("Precion total del producto");
            $table->string("foto")->comment("Foto del producto");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
};
