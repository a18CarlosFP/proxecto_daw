<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    //"idRestaurante","idProducto"
    public function up()
    {
        Schema::create('producto_restaurante', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("restaurante_id");
            $table->unsignedBigInteger("producto_id");
            $table->timestamps();

            $table->foreign("restaurante_id")->references("id")->on("restaurantes")->onDelete('cascade')->onUpdate('cascade');
            $table->foreign("producto_id")->references("id")->on("productos")->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producto_restaurante');
    }
};
