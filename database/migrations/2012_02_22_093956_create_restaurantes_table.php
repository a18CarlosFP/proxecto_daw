<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    //'nombre', 'telefono', 'direccion', 'foto'
    public function up()
    {
        Schema::create('restaurantes', function (Blueprint $table) {
            $table->id();
            $table->string("nombre", 100)->comment("Nombre del restaurante");
            $table->string("telefono", 9)->comment("Telefono del restaurante");
            $table->string("direccion", 150)->comment("Direccion del restaurante");
            $table->string("foto", 150)->comment("Foto del restaurante");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurantes');
    }
};
