@extends ('plantillas.master')

@section('title')
Listado de eventos
@stop

@section('central')

<div class="p-5">


    <h2 class="p-4 bg-white rounded-pill"><center>LISTADO DE RESTAURANTES <a id="btnCreate" href="{{ route('restaurantes.create')}}"><i class="fa-solid fa-circle-plus "></i></a></center></h2>

    @if(Session::has("mensaje"))
    <div class="alert {{ Session::get('alert-class') }}">
        {{Session:get('mensaje')}}
    </div>
    @endif

    
    <div class="container">
        <div class="row">
            

        @foreach ($restaurantes as $restaurante)

        <p>{{URL::asset("fotos/$restaurante->foto")}}</p>
            <div class="col col-xl-4 col-md-6 d-flex justify-content-center align-items-center p-3">
                <div class="card border-dark text-center shadow p-3" style="width: 20rem;border: 3px solid rgb(236, 142, 60)">
                    
                    @if(isset($restaurante->foto) && $restaurante->foto!="foto.png") 
                        <img class="card-img-top rounded-circle" style="border: 5px solid rgb(89, 87, 87)" width="200" height="300" src='{{URL::asset("fotos/$restaurante->foto")}}'>  
                    @else
                        <img class="card-img-top rounded-circle opacity-25"  src='{{URL::asset("img/interrogacion.png")}}' alt="Card image cap">
                    @endif 

                    <div class="card-body  ">
                        <h5 class="card-title p-2 text-white" style="background-color: rgb(196, 190, 188);border-radius:10px">{{$restaurante->nombre}}</h5>
                        <p class="card-text">{{$restaurante->direccion}}</p>
                        
                        <div class="d-flex justify-content-center  justify-content-between">

                            {{-- VER MAS --}}
                            <form action="{{route('restaurante.listaProductos',$restaurante)}}" method="get" enctype='multipart/form-data'>
                                <button type="submit" class="btn btn-primary text-white">
                                    <i class="fa-solid fa-bars"></i>
                                </button>
                            </form>
                            {{-- EDITAR --}}
                            <form action="{{ route('restaurantes.edit', $restaurante) }}" method="get">
                                @csrf
                                @method('PUT')
                                <button type="submit" class="btn btn-warning text-dark">
                                    <i class="fa-solid fa-pen-to-square "></i>
                                </button>
                            </form>

                            {{-- BORRAR --}}
                            <form action="{{ route('restaurantes.destroy', ['restaurante'=>$restaurante->id]) }}" method="post">
                                @csrf
                                @method(' DELETE')
                                <button type="submit" class="btn btn-danger text-white">
                                    <i class="fa-solid fa-trash-can"></i>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
        @endforeach
        </div>
        <div class="row">
            <div class="d-flex justify-content-center">
                <p>{!! $restaurantes->links() !!}</p>
            </div>
        </div>
    </div>

</div>
@stop
