@csrf
<div class="mb-3">
    <label for="nombre" class="form-label">Nombre:</label>
    <input type="text" class="form-control" id="nombre" name="nombre" value="{{ old('nombre',$producto->nombre) }}" maxlength="50">
    @error('nombre')
    <small class='alert alert-danger'>{{ $message }}</small>
    @enderror
</div>


<div class="mb-3">
    <label for="descripcion" class="form-label">Descripcion:</label>
    <input type="text" class="form-control" id="descripcion" name="descripcion" value="{{ old('descripcion',$producto->descripcion) }}" maxlength="200">
    @error('descripcion')
    <small class='alert alert-danger'>{{ $message }}</small>
    @enderror
</div>

<div class="mb-3">
    <label for="precio" class="form-label">Precio:</label>
    <input type="number" class="form-control" id="precio" name="precio" value="{{ old('precio',$producto->precio) }}" maxlength="10">
    @error('precio')
    <small class='alert alert-danger'>{{ $message }}</small>
    @enderror
</div>

<div class="mb-3">
    <label for="foto" class="form-label">Fotografía producto :</label> <strong>{{ old('foto',$producto->foto) }}</strong>
    <input type="file" class="form-control" id="foto" name="foto" value="{{ old('foto',$producto->foto) }}" maxlength="100">
    @if ($errors->has('foto'))
    <small class='alert alert-danger'>{{ $errors->first('foto') }}</small>
    @endif
</div>

