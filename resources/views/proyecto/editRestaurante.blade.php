@extends ('plantillas.master')

@section('title')
Editar restaurante
@stop


@section('central')
    <h2 class=" p-4 bg-white rounded-pill"><center>Editar restaurante</center></h2>

    <div class="p-4">
        <div class=" bg-info rounded-pill p-4">

    <form action="{{ route('restaurantes.update', $restaurante) }}" method="post" enctype="multipart/form-data">
        @method('PUT')
        @include ('proyecto._formRestaurante')

        <div class="d-flex justify-content-center align-item-center">
            <input type="reset" class="btn btn-danger text-white m-2" value="LIMPIAR" />
            <input type="submit" class="btn btn-success text-white m-2" value="EDITAR" />
        </div>
    </form>

@stop