@extends ('plantillas.master')

@section('title')
Editar producto
@stop


@section('central')
<h2>Editar producto</h2>

<form action="{{ route('producto.update', $producto->id) }}" method="post" enctype="multipart/form-data">
    @method('PUT')
    @include ('proyecto._formProducto')

    <input type="reset" class="btn btn-danger" value="Limpiar" />
    <input type="submit" class="btn btn-success" value="Editar" />
</form>

@stop