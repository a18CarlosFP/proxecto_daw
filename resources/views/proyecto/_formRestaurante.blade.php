@csrf

 <div class="row p-2">

    <div class="col-md-1 offset-md-2">
        <span class="text-center text-light"><i class="fa-solid fa-house-chimney fa-2x"></i></span>
    </div>
    <div class="col-md-7 ">
        <input type="text" class="form-control" id="nombre" placeholder="Nombre"  name="nombre" value="{{ old('nombre',$restaurante->nombre) }}" maxlength="50">
        @error('nombre')
        <small class='alert alert-danger'>{{ $message }}</small>
        @enderror   
    </div>
    
</div>

<div class="row p-2">
   
    <div class="col-md-1 offset-md-2">
        <span class="text-center text-light"><i class="fa-solid fa-phone fa-2x"></i></i></span>
    </div>
    <div class="col-md-7 ">
        <input type="text" class="form-control" id="telefono" placeholder="Télefono" name="telefono" value="{{ old('telefono',$restaurante->telefono) }}" maxlength="200">
        @error('telefono')
        <small class='alert alert-danger'>{{ $message }}</small>
        @enderror  
    </div>

</div>

<div class="row p-2">

    <div class="col-md-1 offset-md-2">
        <span class="text-center text-light"><i class="fa-solid fa-map-location fa-2x"></i></i></span>
    </div>
    <div class="col-md-7 ">
        <input type="text" class="form-control" id="direccion" placeholder="Dirección" name="direccion" value="{{ old('direccion',$restaurante->direccion) }}" maxlength="200">
        @error('direccion')
        <small class='alert alert-danger'>{{ $message }}</small>
        @enderror  
    </div>

</div>


<div class="row p-2">

    <div class="col-md-1 offset-md-2">
        <span class="text-center text-light"><i class="fa-solid fa-image fa-2x"></i></span>
    </div>
    <div class="col-md-7 ">
        <input type="file" class="form-control" id="foto" name="foto" value="{{ old('foto',$restaurante->foto) }}" maxlength="100">
        @if ($errors->has('foto'))
        <small class='alert alert-danger'>{{ $errors->first('foto') }}</small>
        @endif
    </div>

</div>
        
