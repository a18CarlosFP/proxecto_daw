@extends ('plantillas.master')

@section('title')
Crear restaurante
@stop

@section('central')


    <h2 class=" p-4 bg-white rounded-pill"><center>Crear restaurante</center></h2>

    <div class="p-4">
        <div class=" bg-info rounded-pill p-4">
            
            <form action="{{ route('restaurantes.store') }}" method="post" enctype="multipart/form-data">

                @include ('proyecto._formRestaurante')

                <div class="d-flex justify-content-center align-item-center">
                    <input type="reset" class="btn btn-danger text-white m-2" value="LIMPIAR" />
                    <input type="submit" class="btn btn-success text-white m-2" value="CREAR" />
                </div>
            </form>
        </div>
    </div>
@stop