@extends ('plantillas.master')

@section('title')
Listado de productos
@stop


@section('central')
<div class="p-5">
    <h2><center>Listado de Productos
        @if(Auth::user()->admin)
            <a href="{{ route('restauranteProducto.añadirProductoRestaurante',$restaurante)}}" class="btn btn-success text-white rounded-circle" title="Añadir Producto">+</a>
        @endif
    </center></h2>

    @if(Session::has("mensaje"))
    <div class="alert {{ Session::get('alert-class') }}">
        {{Session:get('mensaje')}}
    </div>
    @endif


    {{-- <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Descripcion</th>
                <th scope="col">Precio</th>
                <th scope="col">Foto</th>
            </tr>
        </thead>
        <tbody>

            <p>Restaurante -> {{$restaurante->id}}</p>

            @foreach ($listproductos as $producto)
            <tr>
                <th scope="row">{{$producto->id}}</th>
                <td>{{$producto->nombre}}</td>
                <td>{{$producto->descripcion}}</td>
                <td>{{$producto->precio}}</td>
                @if( $producto->foto != "Foto.png")
                <td><img width="200" src='{{ URL::asset("storage/$producto->foto") }}'></td>
                @else
                <td>{{$producto->foto}}</td>
                @endif

                <td>
                    @if(Auth::user()->admin)
                    <form action="{{ route('restauranteProducto.deleteRP', ['restaurante'=>$restaurante->id, 'producto'=>$producto]) }}" method="post">
                        @csrf
                        @method(' DELETE')
                        <input type="submit" class="btn btn-danger text-white" value="Eliminar">
                    </form>
                    <a href="{{ route('producto.edit', $producto) }}" class="btn btn-warning">Modificar</a>
                    @endif
                </td>
            </tr>
            @endforeach


        </tbody>
    </table> --}}
</div>



<div class="container ">
    <div class="col-10 offset-1 ">
        <div class="row">
            
            <div class="col-md-12">
                <div class=" table-responsive mb-0 bg-white p-4 shadow-sm">
                    <table class="table manage-candidates-top mb-0">

                        <thead>
                            <tr>
                                <th class="text-center">Información</th>
                                <th class="text-center">Opinión</th>
                                <th class="action text-right">Precio</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($listproductos as $producto)
                                <tr class="candidates-list">

                                    <td class="title">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-4">
                                                    <img class="img-fluid" width="80" height="80" src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="">
                                                </div>
                                                <div class="col-8">
                                                    <div class="row p-3">
                                                        <div class="col">
                                                            <h5 ><a href="#">{{$producto->nombre}}</a></h5>
                                                        </div>
                                                        <div class="col">
                                                            {{$producto->precio}}<i class="fa-solid fa-euro-sign"></i>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="row">
                                                            <div class="col">
                                                                <i class="fa-light fa-file-lines"></i>{{$producto->descripcion}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </td>

                                    <td class=" text-center">
                                        
                                        <ul class="list-unstyled ">
                                            <li class="p-2">
                                                <i  class="fa-regular fa-thumbs-up"></i><span id='pulgarUp_{{$producto->id}}' class=" order-1"> 0</span>
                                            </li>
                                            <li class="p-2">
                                                <i  class="fa-regular fa-thumbs-down"></i><span id='pulgarDown_{{$producto->id}}' class=" order-1"> 0</span>
                                            </li>                                            

                                        </ul>
                                        
                                    </td>

                                    <td>
                                        <ul class="list-unstyled ">
                                            <li><a href="#" class="text-primary text-center" data-toggle="tooltip" title="" data-original-title="view"><i class="far fa-eye"></i></a></li>
                                            <li><a href="#" class="text-warning text-center" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fas fa-pencil-alt"></i></a></li>
                                            <li><a href="#" class="text-danger text-center" data-toggle="tooltip" title="" data-original-title="Delete"><i class="far fa-trash-alt"></i></a></li>
                                        </ul>
                                    </td>

                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>


