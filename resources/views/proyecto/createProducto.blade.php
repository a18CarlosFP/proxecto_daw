@extends ('plantillas.master')

@section('title')
Crear producto
@stop


@section('central')

<h2>Crear producto</h2>

<form class="m-5 bg-white rounded" action="{{ route('producto.store') }}" method="post" enctype="multipart/form-data">

    <div class="container">
        
            @include ('proyecto._formProducto')
        
    </div>

    <input type="reset" class="btn btn-danger" value="Limpiar" />
    <input type="submit" class="btn btn-success" value="Crear" />
</form>

@stop