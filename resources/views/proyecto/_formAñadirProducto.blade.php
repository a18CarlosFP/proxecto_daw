@extends ('plantillas.master')

@section('title')
Añadir producto
@stop


@section('central')
<h2>Añadir producto:</h2>

<form action="{{ route('restauranteProducto.storeProducto', $restaurante->id) }}" method="post">

    <div class="mb-3">

        <h5 for="titulo" class="form-label">{{$restaurante->nombre}}</h5><br>

        <label for="titulo" class="form-label">Productos:</label>

        <select name="producto_id" class="form-control" id="$producto_id">
            <option value="0">Seleccione un producto</option>
            @foreach ($listaProductos as $producto)
            <option value="{{ $producto->id }}">{{ $producto->nombre}}</option>
            @endforeach
        </select>
        @error('titulo')
        <small class='alert alert-danger'>{{ $message }}</small>
        @enderror

    </div>
    @csrf
    <input type="submit" class="btn btn-success" value="Añadir" />

</form>

@stop