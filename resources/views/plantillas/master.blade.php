<!doctype html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Clase DWCS - 2021-2022">
    <meta name="generator" content="">
    <title>@yield('title')</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/5.1/examples/navbar-fixed/">
    <link  rel="stylesheet" href="{{ asset('css/estilos.css') }}" />
    
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" />

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="/docs/5.1/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="/docs/5.1/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="/docs/5.1/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
    <link rel="manifest" href="/docs/5.1/assets/img/favicons/manifest.json">
    <link rel="mask-icon" href="/docs/5.1/assets/img/favicons/safari-pinned-tab.svg" color="#7952b3">
    <link rel="icon" href="/docs/5.1/assets/img/favicons/favicon.ico">
    <meta name="theme-color" content="#7952b3">

    <script src="https://kit.fontawesome.com/574b37e479.js" crossorigin="anonymous"></script>
    
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

        body {
            min-height: 75rem;
            padding-top: 4.5rem;
            background-color: rgb(236, 233, 233);
            overflow: hidden;
        }

        #listMenu>li{
            margin-top: 5px;
        }

        #nav-bar{
            background-color: rgb(178, 105, 220)
        }
    </style>
</head>

<nav id="nav-bar"class="navbar navbar-expand-sm navbar-dark fixed-top">

    <div class="container-fluid">
      <a class="navbar-brand" href="{{route("inicio")}}"> <i class="fa-solid fa-house fa-2x"></i>  FOOD CHOICE</a>

      <div class="collapse navbar-collapse" id="mynavbar">
        <ul id="listMenu" class="navbar-nav me-auto">

            @auth
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('restaurantes.index') }}">Listado Restaurantes</a>
                </li>
            @endauth

        <!-- <form class="d-flex">
            <input class="form-control me-2" type="text" placeholder="Search">
            <button class="btn btn-primary" type="button">Search</button>
        </form>  -->

      </div>

      <div class="nav-link">
        @auth
            <a class="text-white p-2" style="text-decoration: none;" href="#"><i class="fa-solid fa-user fa-2x"></i> {{Auth::user()->name}} </a>
            <a class="text-white p-2" href="#"> <i class="fa-solid fa-cart-shopping fa-2x"></i></a>
            <a href="#" style="text-decoration: none;"style="text-decoration: none;" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fa-solid fa-arrow-right-from-bracket fa-2x"></i></a>
           
        @endauth

        @guest
            <a class="text-white p-2" style="text-decoration: none;" href="{{ route('register') }}">Registrarse</a>
            <a class="text-white p-2" style="text-decoration: none;" href="{{ route('login') }}">Login</a>
        @endguest

        </div>
    </div>
  </nav>


<body>
    
    <main class="container-fluid">

        <div id="contenido" class=" rounded">
            @yield('central')
        </div>
    </main>

    <form method="POST" id="logout-form" action="{{ route('logout') }}">
        @csrf
    </form>

    <footer class="text-center text-white p-2 fixed-bottom" style="background-color: rgb(170, 166, 166);">
        © 2022 Proyecto 2ºDAW:
        <a class="text-white" href="#">Carlos J Fuentes Puga</a>
        <p><i class="fa-solid fa-envelope"></i> a18carlosfp@iessanclemente.net</p>
    </footer>

    <script src="{{ asset('js/app.js') }}"></script>

</body>




 
    
</html>


    {{-- <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">FOOD CHOICE</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav me-auto mb-2 mb-md-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="{{ route('inicio') }}">Inicio</a>
                    </li>
                    @auth
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('restaurantes.index') }}">Listado Restaurantes</a>
                    </li>
                    @if( Auth::user()->admin )
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('producto.create')}}">Añadir nuevo producto</a>
                    </li>
                    @endif
                    <li class="nav-item">
                        <a class="nav-link" href="#" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Desconectar</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#"><b>{{auth()->user()->name}}</b></a>
                    </li>
                    @endauth
                    @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">Registrarse</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">Login</a>
                    </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav> --}}