<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProducto;
use App\Http\Requests\StoreProductoRestaurante;
use App\Models\Producto;
use App\Models\Restaurante;
use Illuminate\Http\Request;
use App\Models\RestauranteProducto;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\StoreRestauranteProducto;
use App\Models\ProductoRestaurante;
use Illuminate\Support\Facades\DB;

class RestauranteProductoController extends Controller
{

    public function añadirProductoRestaurante(Restaurante $restaurante)
    {
        $listaProductos = Producto::all();
        
        return view("proyecto._formAñadirProducto", compact("restaurante", "listaProductos"));
    }


    public function deleteRP($id, Producto $producto)
    {

        $restauranteProducto = ProductoRestaurante::where('restaurante_id', $id)->where('producto_id', $producto->id)->first();
        $restauranteProducto->delete();

        Session::flash("mensaje", "Producto borrado correctamente");
        Session::flash("alert-class", "alert-success");

        return redirect()->route("inicio");
    }

    public function storeProducto(Restaurante $restaurante, StoreProductoRestaurante $request){

        //dd($restaurante);
        //dd($request);
        $datosvalidados = $request->validated();
        $datosvalidados['restaurante_id'] = $restaurante->id;

        // Almacenamos en la base de datos
        ProductoRestaurante::create($datosvalidados);

        // Guarde un mensaje Flash.
        Session::flash('mensaje', 'Evento dado de alta correctamente.');
        Session::flash('alert-class', 'alert-success');

        return redirect()->route('inicio');

    }
}
