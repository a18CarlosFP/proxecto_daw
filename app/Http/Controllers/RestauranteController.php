<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use App\Models\Restaurante;
use Illuminate\Http\Request;
use App\Models\RestauranteProducto;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRestaurante;
use Illuminate\Support\Facades\Session;

class RestauranteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $restaurante = Restaurante::all();
        // return view('proyecto.index')->with('restaurantes', $restaurante);

        $restaurante = Restaurante::paginate(3);    

    	return view('proyecto.index')->with('restaurantes', $restaurante);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("proyecto.createRestaurante")->with("restaurante", new Restaurante());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRestaurante $request)
    {
       
        $data = $request->validated();
        
        // Almacenamos el archivo recibido en el servidor
        if (isset($data['foto'])) {
            $filename=time().".".$data['foto']->extension();
            $data['foto']= $filename;

            $request->foto->move(public_path("fotos"),$filename);
        }else{
            $data['foto']= "foto.png";
        }

        // Almacenamos en la base de datos
        Restaurante::create($data);

        // Guarde un mensaje Flash.
        Session::flash('mensaje', 'Evento dado de alta correctamente.');
        Session::flash('alert-class', 'alert-success');

        return redirect()->route('inicio');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Restaurante $restaurante)
    {
        return view("proyecto.editRestaurante")->with("restaurante", $restaurante);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreRestaurante $request,Restaurante $restaurante)
    {
        // Actualizamos el evento.
        $data = $request->validated();
        
        // Almacenamos el archivo recibido en el servidor
        if (isset($data['foto'])) {
            $filename=time().".".$data['foto']->extension();
            $data['foto']= $filename;

            $request->foto->move(public_path("fotos"),$filename);
        }else{
            $data['foto']= "foto.png";
        }

        $restaurante->update($data);

        // Guarde un mensaje Flash.
        Session::flash('mensaje', 'Evento actualizado correctamente.');
        Session::flash('alert-class', 'alert-success');

        // Redireccionamos al index.
        return redirect()->route('inicio');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Restaurante $restaurante)
    {
        $restaurante->delete();

        Session::flash("mensaje", "Producto borrado correctamente");
        Session::flash("alert-class", "alert-success");

        return redirect()->route("index");
    }

    function listaProductos(Restaurante $restaurante)
    {
        $listproductos = $restaurante->productos()->get();
        //dd($listproductos);
        return view('proyecto.listarProductos', compact('listproductos', 'restaurante')); //->with('productos',$listproductos);

        /*$productos=$restaurante->producto();
        return view('proyecto.listarProductos')->with('productos',$productos);*/
    }
}
