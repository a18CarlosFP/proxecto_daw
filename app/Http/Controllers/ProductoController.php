<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProducto;
use App\Models\Producto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productos = Producto::all();
        dd($productos[1]->nombre);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("proyecto.createProducto")->with("producto", new Producto());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProducto $request)
    {
        $datosvalidados = $request->validated();
        $datosvalidados['foto'] = 'foto.png';

        // Almacenamos el archivo recibido en el servidor
        if ($request->file('foto')) {
            $fichero = $request->file('foto');
            $nuevonombre = time() . '_' . $fichero->getClientOriginalName();

            $ruta = $request->foto->store('fotos', 'public');
            $datosvalidados['foto'] = $ruta;
            echo $ruta;
        }

        // Almacenamos en la base de datos
        Producto::create($datosvalidados);

        // Guarde un mensaje Flash.
        Session::flash('mensaje', 'Evento dado de alta correctamente.');
        Session::flash('alert-class', 'alert-success');

        return redirect()->route('inicio');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Producto $producto)
    {
        //dd($producto);
        return view('proyecto.editarProducto')->with('producto', $producto);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreProducto $request, Producto  $producto)
    {
        // Actualizamos el evento.
        $producto->update($request->validated());

        // Guarde un mensaje Flash.
        Session::flash('mensaje', 'Evento actualizado correctamente.');
        Session::flash('alert-class', 'alert-success');

        // Redireccionamos al index.
        return redirect()->route('inicio');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Producto $producto)
    {

        //$producto->delete();

        Session::flash("mensaje", "Producto borrado correctamente");
        Session::flash("alert-class", "alert-success");

        return redirect()->route("index");
    }

 }


