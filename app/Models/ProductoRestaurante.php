<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ProductoRestaurante extends Pivot
{
    use HasFactory;

    protected $fillable = array('restaurante_id', 'producto_id');

}
