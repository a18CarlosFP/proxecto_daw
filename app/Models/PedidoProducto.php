<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PedidoProducto extends Pivot
{
    use HasFactory;

    protected $fillable=array("idPedido","idProducto");
}
