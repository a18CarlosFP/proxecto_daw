<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    use HasFactory;

    protected $fillable = array('fechaHora_pedido','fechaHora_entrega');


    public function restaurante(){

        return $this->hasMany(Pedido::class);
    }
}
