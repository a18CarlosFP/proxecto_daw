<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Restaurante extends Model
{
    use HasFactory;

    protected $fillable = array('nombre', 'telefono', 'direccion', 'foto');


    // public function pedido()
    // {

    //     return $this->belongsTo(Restaurante::class);
    // }

    public function productos()
    {

        return $this->belongsToMany(Producto::class);
    }
}
