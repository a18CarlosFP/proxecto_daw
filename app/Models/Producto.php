<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    use HasFactory;

    protected $fillable = array('nombre', 'descripcion', 'precio', 'foto');


    public function restaurantes()
    {
        return $this->belongsTo(Restaurante::class);
    }

    // public function pedido()
    // {
    //     return $this->hasMany(Producto::class);
    // }
}
